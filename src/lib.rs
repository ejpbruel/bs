use {
    rand::{rngs::StdRng, Rng, SeedableRng},
    std::{
        char,
        collections::HashMap,
        fs::{self, File, OpenOptions},
        io::{self, Read, Seek, SeekFrom, Write},
        mem,
        path::{Path, PathBuf},
    },
};

#[derive(Debug)]
pub struct Bs {
    path: PathBuf,
    blocks: HashMap<Id, Block>,
    rng: StdRng,
}

impl Bs {
    pub fn open<P>(path: P) -> Result<Bs, Error>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref().to_owned();
        let mut blocks = HashMap::new();
        let mut index_path = path.clone();
        index_path.push("index");
        let index_buf = fs::read(index_path)?;
        let mut index_buf_slice = index_buf.as_slice();
        while !index_buf_slice.is_empty() {
            let mut id = [0; 16];
            index_buf_slice.read_exact(&mut id).unwrap();
            let mut bytes = [0; 8];
            index_buf_slice.read_exact(&mut bytes).unwrap();
            let len = u64::from_le_bytes(bytes);
            blocks.insert(
                id,
                Block {
                    len,
                    buf: Vec::new(),
                },
            );
        }
        Ok(Bs {
            path,
            blocks,
            rng: StdRng::from_entropy(),
        })
    }

    pub fn create(&mut self) -> Result<Id, Error> {
        let id = self.rng.gen();
        self.blocks.insert(
            id,
            Block {
                len: 0,
                buf: Vec::new(),
            },
        );
        Ok(id)
    }

    pub fn read_exact(&mut self, id: Id, buf: &mut [u8]) -> Result<(), Error> {
        let block = self.blocks.get(&id).ok_or(Error)?;
        let buf_len = buf.len();
        let block_len = block.len as usize;
        if buf_len > block_len + block.buf.len() {
            return Err(Error);
        }
        let mut file = File::open(block_path(&self.path, id))?;
        file.read_exact(&mut buf[..buf_len.min(block_len)])?;
        if buf_len > block_len {
            buf[block_len..].copy_from_slice(&block.buf);
        }
        Ok(())
    }

    pub fn write_all(&mut self, id: Id, buf: &[u8]) -> Result<(), Error> {
        let block = self.blocks.get_mut(&id).ok_or(Error)?;
        block.buf.extend_from_slice(buf);
        Ok(())
    }

    pub fn sync_all(&mut self) -> Result<(), Error> {
        for (id, block) in &mut self.blocks {
            if block.buf.is_empty() {
                continue;
            }
            let mut block_file = OpenOptions::new()
                .write(true)
                .open(block_path(&self.path, *id))?;
            block_file.seek(SeekFrom::Start(block.len))?;
            block_file.write_all(&block.buf)?;
            block_file.sync_all()?;
            block.len += block.buf.len() as u64;
            block.buf.clear();
        }
        let mut index_buf = Vec::new();
        for (id, block) in &self.blocks {
            index_buf.write_all(id).unwrap();
            let bytes = block.len.to_le_bytes();
            index_buf.write_all(&bytes).unwrap();
        }
        let dir = File::open(&self.path)?;
        let mut tmp_index_path = self.path.clone();
        tmp_index_path.push("index.tmp");
        fs::write(&tmp_index_path, index_buf)?;
        let mut index_path = self.path.clone();
        index_path.push("index");
        fs::rename(tmp_index_path, index_path)?;
        dir.sync_all()?;
        Ok(())
    }
}

pub type Id = [u8; 16];

#[derive(Clone, Copy)]
pub struct Error;

impl From<io::Error> for Error {
    fn from(_error: io::Error) -> Error {
        Error
    }
}

#[derive(Clone, Debug)]
struct Block {
    len: u64,
    buf: Vec<u8>,
}

fn block_path<P>(base: P, id: Id) -> PathBuf
where
    P: AsRef<Path>,
{
    let mut path = base.as_ref().to_owned();
    let mut string = String::with_capacity(mem::size_of::<Id>());
    for byte in &id {
        string.push(char::from_digit(*byte as u32, 16).unwrap());
    }
    path.reserve(string.len());
    path.push(&string[..3]);
    path.push(&string[3..]);
    path
}
